require 'sinatra'
require 'redis'
require 'yammer'
require 'pry'
require 'json'

redis = Redis.new

Yammer.configure do |c|
  c.client_id= 'wSwfvvIDByfzK1DOqYNKw'
  c.client_secret = 'MSwqUkF2KTyziQ4hmolxerSvNdS7ITDfr1EseAXA5lI&code=O2KCYjZw5nKxWessbBRdw'
  c.access_token = "gleFHSvSYmLibCH4YL2zzQ"
end

user = Yammer.current_user

def get_risk_model(redis,risk)
  kids = [] 
  keys = redis.keys '*'
  keys.each do |key|
    subname = key[risk.size + 2..-1]
    type = redis.type(key)	
    items = nil
    case type
      when 'set'
        items = redis.smembers key	
      when 'hash'
        items = redis.hgetall key	
      else
       puts type
    end 
    items = items.map{|x| {"name" => x } }
    items = {'name' => subname, 'children' => items } 
    kids << items
  end
  results = {'name' => risk, 'children' => kids }
  return results.to_json
end

get "/risk/:name" do
	get_risk_model redis, params[:name]
end

get "/messages/:group/:when" do
end

post "message/:group/:text" do
end

get "/" do 
  redirect "index.html"    
end

post "/sadd/:key/:member" do
	redis.sadd params[:key].strip, params[:member].strip
end

get "/smembers/:key" do
	redis.smembers(params[:key].strip).to_json
end

delete "/srem/:key/:member" do
	redis.srem params[:key].strip, params[:member].strip
end


get "/hget/:key" do
	redis.hget(params[:key]).to_json
end

post "/hset/:key/:field" do
	redis.hset params[:key], params[:field]
end

get "/hgetall/:key" do
	redis.hgetll(params[:key]).to_json
end

delete "/hdel/:key/:field" do
	redis.hdel params[:key], params[:field]
end
