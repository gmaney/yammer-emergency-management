#!/usr/local/bin/ruby 
require 'redis'
require 'pry'
require 'json'

sets = #w{People Places Properties}
hashes = #w{Placment Presence}

other = #{Routes Refuges Roles Resources}

hashes = {
	:placement => {:property => :place},
	:presence => {:person => :place}
}

path = File.expand_path(File.dirname(__FILE__)) + '/' + ARGV[0]
lines = File.open(path).readlines
risk = ARGV[0].split('.')[0];
risk = risk.gsub(' ','_');
redis = Redis.new

%w{Places Properties Paths Presence Placement Passable Perilous}.each do |name|
	redis.del "#{risk}--#{name}"
end

=begin
People
Places
Properties
Presences
Placements
Routes 
Refuges
Roles
Resposibilities
=end

def process_content(line,context,redis,risk)
  case context 
    when nil
      raise 'oops'
    when 'People', 'Places','Properties','Paths', 'Passible','Perilous'
      redis.sadd "#{risk}--#{context}", "#{line}"
    when 'Routes', 'Refuges'
      redis.sadd "#{risk}--#{context}", "#{line}"
    when 'Responsibilities', 'Roles'
      pair = line.split(':',2);	
      redis.hset "#{risk}--#{context}", "#{pair[0].strip}", "#{pair[1].strip}"
    when 'Presences', 'Placements'
      pair = line.split(':',2);
      redis.hset "#{risk}--#{context}", "#{pair[0].strip}", "#{pair[1].strip}"
    else
	raise 'oops: ' + context
  end
end

def process_lines(lines,redis,risk)
  context = nil
  (0...lines.count).each do |i|
    line = lines[i] 
    if line[0] == "\t"
      process_content(line.strip, context, redis, risk)
    else
      context = line.strip	
    end
  end
end
process_lines(lines,redis,risk)

def dump(redis)
  results = {}
  keys = redis.keys '*'
  keys.each do |key|
    type = redis.type(key)	
    case type
      when 'set'
        results[key] = redis.smembers key	
      when 'hash'
        results[key] = redis.hgetall key	
      else
       puts type
    end 
  end
  return results.to_json
end

def dump2(redis,risk)
  kids = [] 
  keys = redis.keys '*'
  keys.each do |key|
    subname = key[risk.size + 2..-1]
    type = redis.type(key)	
    items = nil
    case type
      when 'set'
        items = redis.smembers key	
      when 'hash'
        items = redis.hgetall key	
      else
       puts type
    end 
    items = items.map{|x| {"name" => x } }
    items = {'name' => subname, 'children' => items } 
    kids << items
  end
  results = {'name' => risk, 'children' => kids }
  return results.to_json
end

puts dump2(redis,risk)
