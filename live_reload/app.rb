require 'sinatra'
require "rack/livereload"

use Rack::LiveReload

get "/" do 
  redirect "index.html"    
end
